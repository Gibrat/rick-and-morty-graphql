part of './pages.dart';

abstract class Routes {
  static const CHARACTERS_PAGE = '/characters';
  static const CHARACTER_DETAIL_PAGE = '/characters/:id';
  static String characterDetailPage(String id) => '/characters/$id';
}
