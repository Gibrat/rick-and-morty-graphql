import 'package:get/get.dart';
import 'package:test_graphql/features/character_details/bindings/character_details_bindings.dart';
import 'package:test_graphql/features/character_details/view/character_details_page.dart';
import 'package:test_graphql/features/list_of_characters/bindings/list_of_characters_bindings.dart';
import 'package:test_graphql/features/list_of_characters/view/list_of_characters_page.dart';
part './routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(
      name: Routes.CHARACTERS_PAGE,
      binding: ListOfCharactersBinding(),
      page: () => const ListOfCharactersPage(),
    ),
    GetPage(
      name: Routes.CHARACTER_DETAIL_PAGE,
      binding: CharacterDetailBinding(),
      page: () => const CharacterDetailPage(),
    )
  ];
}
