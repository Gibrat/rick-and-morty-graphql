import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CacheImage extends StatelessWidget {
  final String? imageUrl;
  final double? width, height;
  final BoxFit? fit;
  final BoxDecoration? boxDecoration;
  final String? imageEmptyPath;
  final EdgeInsets? padding;
  const CacheImage(this.imageUrl,
      {this.width,
      Key? key,
      this.height,
      this.padding,
      this.fit,
      this.boxDecoration,
      this.imageEmptyPath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (imageUrl == null) {
      return _errorWidget();
    }

    return CachedNetworkImage(
      fit: fit,
      width: width,
      height: height,
      imageUrl: imageUrl ?? '',
      imageBuilder: (context, imageProvider) {
        return _imageWidget(imageProvider, context);
      },
      placeholder: (context, url) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
      errorWidget: (context, url, error) {
        return _errorWidget();
      },
    );
  }

  Widget _errorWidget() {
    return Container(
      width: width,
      height: height,
      padding: padding,
      decoration: boxDecoration,
      child: Image.asset('assets/images/empty_image.png', fit: fit),
    );
  }

  Widget _imageWidget(ImageProvider imageProvider, context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: boxDecoration != null
          ? boxDecoration!
              .copyWith(image: DecorationImage(image: imageProvider, fit: fit))
          : BoxDecoration(
              image: DecorationImage(image: imageProvider, fit: fit)),
      child: Container(decoration: boxDecoration),
    );
  }
}
