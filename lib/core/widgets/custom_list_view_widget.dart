import 'package:flutter/material.dart';

class CustomListViewWidget extends StatefulWidget {
  final bool isLoading;
  final String? error;
  final bool isEmpty;
  final Widget child;
  final Function nextLoad;
  final Function reload;
  final EdgeInsets? padding;
  const CustomListViewWidget(
      {Key? key,
      required this.isLoading,
      required this.isEmpty,
      this.error,
      required this.child,
      required this.nextLoad,
      required this.reload,
      this.padding})
      : super(key: key);

  @override
  _CustomListViewWidgetState createState() => _CustomListViewWidgetState();
}

class _CustomListViewWidgetState extends State<CustomListViewWidget> {
  late ScrollController scrollController;

  @override
  void initState() {
    scrollController = ScrollController();

    setupScrollController();
    super.initState();
  }

  void setupScrollController() {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          widget.nextLoad();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final showChild = !widget.isEmpty;
    final showNowDate =
        widget.error == null && !widget.isLoading && widget.isEmpty;
    final showError = widget.error != null && widget.error!.isNotEmpty;

    return RefreshIndicator(
      onRefresh: () async => widget.reload(),
      child: ListView(
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        controller: scrollController,
        children: [
          if (showChild)
            Padding(
              padding: widget.padding ??
                  const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: widget.child,
            ),
          if (showNowDate)
            Container(
              margin: const EdgeInsets.only(top: 20),
              child: const Text("Нет данных", textAlign: TextAlign.center),
            ),
          if (showError)
            Column(children: [
              const SizedBox(height: 20),
              Text(widget.error!),
              const SizedBox(height: 20),
              ElevatedButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                      const EdgeInsets.symmetric(vertical: 6, horizontal: 10)),
                ),
                onPressed: () => widget.reload(),
                child: const Text("Обновить", textAlign: TextAlign.center),
              ),
            ]),
          if (widget.isLoading)
            Container(
              margin: const EdgeInsets.only(top: 20),
              width: MediaQuery.of(context).size.width,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            ),
          const SizedBox(height: 40),
        ],
      ),
    );
  }
}
