import 'package:get/get.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class HttpService extends GetxService {
  late GraphQLClient _client;

  Future<HttpService> init() async {
    HttpLink link = HttpLink('https://rickandmortyapi.com/graphql/');

    _client = GraphQLClient(link: link, cache: GraphQLCache());
    return this;
  }

  Future<QueryResult> performQuery(String query,
      {required Map<String, dynamic> variables}) async {
    final options = QueryOptions(document: gql(query), variables: variables);
    final result = await _client.query(options);
    return result;
  }

  Future<QueryResult> performMutation(String query,
      {required Map<String, dynamic> variables}) async {
    final options = MutationOptions(document: gql(query), variables: variables);
    final result = await _client.mutate(options);
    return result;
  }
}
