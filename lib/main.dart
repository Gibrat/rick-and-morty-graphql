import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_graphql/core/routes/pages.dart';

import 'locator_service.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await locatorService();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: Routes.CHARACTERS_PAGE,
        getPages: AppPages.pages);
  }
}
