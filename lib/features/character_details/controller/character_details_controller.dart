import 'package:get/get.dart';
import 'package:test_graphql/domain/data/characters_remote_data_source.dart';
import 'package:test_graphql/domain/repository/characters_repository.dart';

import '../../../domain/entity/character_entity.dart';

class _ViewModelState {
  bool isLoading = false;
  CharacterEntity? characterEntity;
  String? errorText;
}

class CharacterDetailController extends GetxController {
  final CharactersRepository _repository;
  CharacterDetailController(this._repository);

  final _state = _ViewModelState();
  _ViewModelState get state => _state;

  @override
  void onInit() {
    loadCharacter();
    super.onInit();
  }

  Future<void> loadCharacter() async {
    final id = Get.parameters['id'];
    if (_state.isLoading || id == null) return;
    try {
      final characterId = int.parse(id);
      _state.isLoading = true;
      _state.errorText = '';
      update();
      final response = await _repository.getCharacter(characterId);
      _state.characterEntity = response;
    } on DefaultAppFailure catch (e) {
      _state.errorText = e.message;
    } catch (e) {
      _state.errorText = e.toString();
    } finally {
      _state.isLoading = false;
      update();
    }
  }
}
