import 'package:get/get.dart';

import '../controller/character_details_controller.dart';

class CharacterDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<CharacterDetailController>(CharacterDetailController(Get.find()));
  }
}
