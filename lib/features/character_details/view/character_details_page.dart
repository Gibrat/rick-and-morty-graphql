import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_graphql/core/widgets/cache_image_widget.dart';
import 'package:test_graphql/core/widgets/custom_list_view_widget.dart';

import '../controller/character_details_controller.dart';

class CharacterDetailPage extends GetView<CharacterDetailController> {
  const CharacterDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CharacterDetailController>(
      initState: (_) {},
      builder: (controller) {
        return Scaffold(
            appBar: AppBar(
                title: Text(controller.state.characterEntity?.name ?? '')),
            body: CustomListViewWidget(
                isEmpty: controller.state.characterEntity == null,
                isLoading: controller.state.isLoading,
                error: controller.state.errorText,
                nextLoad: () {},
                padding: EdgeInsets.zero,
                reload: controller.loadCharacter,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CacheImage(
                      controller.state.characterEntity?.image,
                      width: double.infinity,
                      height: 200,
                      fit: BoxFit.contain,
                    ),
                    Text("Name: ${controller.state.characterEntity?.name}"),
                    Text("gender: ${controller.state.characterEntity?.gender}"),
                    Text("status: ${controller.state.characterEntity?.status}"),
                    if (controller.state.characterEntity?.episode != null) ...[
                      const Text('Episodes: '),
                      ...controller.state.characterEntity!.episode!
                          .map((e) => Text(e.episode.toString()))
                    ]
                  ],
                )));
      },
    );
  }
}
