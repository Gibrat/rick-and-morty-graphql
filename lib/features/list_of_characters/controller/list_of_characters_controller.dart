import 'package:get/get.dart';
import 'package:test_graphql/core/routes/pages.dart';
import 'package:test_graphql/domain/data/characters_remote_data_source.dart';
import 'package:test_graphql/domain/entity/character_entity.dart';
import 'package:test_graphql/domain/repository/characters_repository.dart';

class _ViewModelState {
  final _isLoading = false.obs;
  bool get isLoading => _isLoading.value;
  set isLoading(bool load) => _isLoading.value = load;

  final _listCharacters = <CharacterEntity>[].obs;
  List<CharacterEntity> get listCharacters => _listCharacters.value;
  set listCharacters(List<CharacterEntity> newList) =>
      _listCharacters.value = newList;

  final _errorText = ''.obs;
  String get errorText => _errorText.value;
  set errorText(String text) => _errorText.value = text;

  int? nextPage = 1;
}

class ListOfCharactersController extends GetxController {
  final CharactersRepository _repository;
  ListOfCharactersController(this._repository);

  final _state = _ViewModelState();
  _ViewModelState get state => _state;

  @override
  void onInit() {
    firstLoadCharacters();
    super.onInit();
  }

  void onPressedCharacter(String? id) {
    if (id == null) return;
    Get.toNamed(Routes.characterDetailPage(id));
  }

  Future<void> firstLoadCharacters() async {
    if (_state.isLoading) return;
    try {
      _state.isLoading = true;
      _state.errorText = '';
      _state.nextPage = 1;
      final response = await _repository.getCharacters(_state.nextPage!);
      state.listCharacters = response.results ?? [];
      _state.nextPage = response.info?.next;
    } on DefaultAppFailure catch (e) {
      _state.errorText = e.message;
    } catch (e) {
      _state.errorText = e.toString();
    } finally {
      _state.isLoading = false;
    }
  }

  Future<void> nextLoadCharacters() async {
    if (_state.isLoading || _state.nextPage == null) return;
    try {
      _state.isLoading = true;
      _state.errorText = '';
      final response = await _repository.getCharacters(_state.nextPage!);
      state.listCharacters = [
        ...state.listCharacters,
        ...response.results ?? []
      ];
      _state.nextPage = response.info?.next;
    } on DefaultAppFailure catch (e) {
      _state.errorText = e.message;
    } catch (e) {
      _state.errorText = e.toString();
    } finally {
      _state.isLoading = false;
    }
  }
}
