import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:test_graphql/core/widgets/cache_image_widget.dart';
import 'package:test_graphql/core/widgets/custom_list_view_widget.dart';

import '../controller/list_of_characters_controller.dart';
import 'widget/character_card_widget.dart';

class ListOfCharactersPage extends GetView<ListOfCharactersController> {
  const ListOfCharactersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Rick and Morty')),
        body: Obx(() {
          final state = controller.state;
          return CustomListViewWidget(
              isLoading: state.isLoading,
              error: state.errorText,
              isEmpty: state.listCharacters.isEmpty,
              nextLoad: controller.nextLoadCharacters,
              reload: controller.firstLoadCharacters,
              child: Column(
                children: [
                  ...state.listCharacters.map(
                      (character) => CharacterCardWidget(character: character))
                ],
              ));
        }));
  }
}
