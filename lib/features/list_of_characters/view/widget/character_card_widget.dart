import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:test_graphql/core/widgets/cache_image_widget.dart';
import 'package:test_graphql/domain/entity/character_entity.dart';

import '../../controller/list_of_characters_controller.dart';

class CharacterCardWidget extends StatelessWidget {
  final CharacterEntity character;
  const CharacterCardWidget({Key? key, required this.character})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<ListOfCharactersController>();
    Widget gender = Container();
    if (character.gender == 'Male') {
      gender = SvgPicture.asset('assets/icons/male.svg', height: 20, width: 20);
    }
    if (character.gender == 'Female') {
      gender =
          SvgPicture.asset('assets/icons/female.svg', height: 20, width: 20);
    }

    return ListTile(
      onTap: () => controller.onPressedCharacter(character.id),
      leading: CacheImage(
        character.image,
        height: 80,
        width: 80,
      ),
      title: Text(character.name.toString()),
      subtitle: Row(
        children: [gender, Text(character.gender.toString())],
      ),
    );
  }
}
