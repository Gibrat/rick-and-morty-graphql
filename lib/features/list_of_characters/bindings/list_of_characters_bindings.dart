import 'package:get/get.dart';

import '../controller/list_of_characters_controller.dart';

class ListOfCharactersBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<ListOfCharactersController>(ListOfCharactersController(Get.find()));
  }
}
