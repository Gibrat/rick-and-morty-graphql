import 'package:get/get.dart';
import 'package:test_graphql/core/services/http_service.dart';
import 'package:test_graphql/domain/data/characters_remote_data_source.dart';
import 'package:test_graphql/domain/repository/characters_repository.dart';

Future<void> locatorService() async {
  await Get.putAsync(() => HttpService().init(), permanent: true);

  Get.lazyPut(() => CharactersRepository(CharactersRemoteDataSource()),
      fenix: true);
}
