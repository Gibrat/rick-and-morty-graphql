import 'package:get/get.dart';
import 'package:test_graphql/core/services/http_service.dart';

import '../entity/character_entity.dart';
import '../entity/characters_list_entity.dart';

class CharactersRemoteDataSource {
  late HttpService _httpService;

  CharactersRemoteDataSource() {
    _httpService = Get.find<HttpService>();
  }

  Future<CharactersListEntity> getCharacters(int page) async {
    final response =
        await _httpService.performQuery("""query readPage(\$page: Int!) {
                  characters(page: \$page) {
                    info {
                      count,
                      pages,
                      next
                    },
                    results{
                      id
                      name
                      gender
                      image
                    }
                  }
                }
          """, variables: {"page": page});

    if (response.hasException) {
      throw DefaultAppFailure(response.exception.toString());
    }
    return CharactersListEntity.fromJson(response.data!['characters']!);
  }

  Future<CharacterEntity> getCharacter(int id) async {
    final response = await _httpService.performQuery("""
                    query readPage(\$idCharacter: ID!){
                      character(id: \$idCharacter) {
                          id
                          name
                          gender
                          image
                          status
                          episode {
                            episode
                          }
                      }
                    }
          """, variables: {"idCharacter": id});

    if (response.hasException) {
      throw DefaultAppFailure(response.exception.toString());
    }
    return CharacterEntity.fromJson(response.data!['character']!);
  }
}

class DefaultAppFailure {
  final String message;

  DefaultAppFailure(this.message);
}
