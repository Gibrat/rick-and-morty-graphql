import '../data/characters_remote_data_source.dart';
import '../entity/character_entity.dart';
import '../entity/characters_list_entity.dart';

class CharactersRepository {
  final CharactersRemoteDataSource _charactersRemoteDataSource;

  CharactersRepository(this._charactersRemoteDataSource);

  Future<CharactersListEntity> getCharacters(int page) async =>
      await _charactersRemoteDataSource.getCharacters(page);

  Future<CharacterEntity> getCharacter(int id) async =>
      await _charactersRemoteDataSource.getCharacter(id);
}
