import 'package:json_annotation/json_annotation.dart';

part 'episode_entity.g.dart';

@JsonSerializable()
class EpisodeEntity {
  EpisodeEntity();
  @JsonKey(name: 'episode')
  String? episode;

  factory EpisodeEntity.fromJson(Map<String, dynamic> json) =>
      _$EpisodeEntityFromJson(json);
  Map<String, dynamic> toJson() => _$EpisodeEntityToJson(this);
}
