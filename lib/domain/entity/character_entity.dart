import 'package:json_annotation/json_annotation.dart';

import 'episode_entity.dart';
part 'character_entity.g.dart';

@JsonSerializable()
class CharacterEntity {
  CharacterEntity();

  @JsonKey(name: 'id')
  String? id;

  @JsonKey(name: 'name')
  String? name;

  @JsonKey(name: 'gender')
  String? gender;

  @JsonKey(name: 'image')
  String? image;

  @JsonKey(name: 'status')
  String? status;

  @JsonKey(name: 'episode')
  List<EpisodeEntity>? episode;

  factory CharacterEntity.fromJson(Map<String, dynamic> json) =>
      _$CharacterEntityFromJson(json);
  Map<String, dynamic> toJson() => _$CharacterEntityToJson(this);
}
