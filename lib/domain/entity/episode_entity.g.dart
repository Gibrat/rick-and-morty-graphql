// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'episode_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EpisodeEntity _$EpisodeEntityFromJson(Map<String, dynamic> json) =>
    EpisodeEntity()..episode = json['episode'] as String?;

Map<String, dynamic> _$EpisodeEntityToJson(EpisodeEntity instance) =>
    <String, dynamic>{
      'episode': instance.episode,
    };
