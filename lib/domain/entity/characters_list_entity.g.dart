// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'characters_list_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharactersListEntity _$CharactersListEntityFromJson(
        Map<String, dynamic> json) =>
    CharactersListEntity()
      ..info = json['info'] == null
          ? null
          : PaginationInfoEntity.fromJson(json['info'] as Map<String, dynamic>)
      ..results = (json['results'] as List<dynamic>?)
          ?.map((e) => CharacterEntity.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$CharactersListEntityToJson(
        CharactersListEntity instance) =>
    <String, dynamic>{
      'info': instance.info,
      'results': instance.results,
    };
