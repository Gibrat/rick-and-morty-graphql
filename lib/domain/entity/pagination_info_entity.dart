import 'package:json_annotation/json_annotation.dart';

part 'pagination_info_entity.g.dart';

@JsonSerializable()
class PaginationInfoEntity {
  PaginationInfoEntity();

  @JsonKey(name: 'count')
  int? count;
  @JsonKey(name: 'pages')
  int? pages;
  @JsonKey(name: 'next')
  int? next;

  factory PaginationInfoEntity.fromJson(Map<String, dynamic> json) =>
      _$PaginationInfoEntityFromJson(json);
  Map<String, dynamic> toJson() => _$PaginationInfoEntityToJson(this);
}
