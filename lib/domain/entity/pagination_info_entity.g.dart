// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pagination_info_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaginationInfoEntity _$PaginationInfoEntityFromJson(
        Map<String, dynamic> json) =>
    PaginationInfoEntity()
      ..count = json['count'] as int?
      ..pages = json['pages'] as int?
      ..next = json['next'] as int?;

Map<String, dynamic> _$PaginationInfoEntityToJson(
        PaginationInfoEntity instance) =>
    <String, dynamic>{
      'count': instance.count,
      'pages': instance.pages,
      'next': instance.next,
    };
