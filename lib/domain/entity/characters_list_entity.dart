import 'package:json_annotation/json_annotation.dart';

import 'character_entity.dart';
import 'pagination_info_entity.dart';

part 'characters_list_entity.g.dart';

@JsonSerializable()
class CharactersListEntity {
  CharactersListEntity();

  @JsonKey(name: 'info')
  PaginationInfoEntity? info;
  @JsonKey(name: 'results')
  List<CharacterEntity>? results;

  factory CharactersListEntity.fromJson(Map<String, dynamic> json) =>
      _$CharactersListEntityFromJson(json);
  Map<String, dynamic> toJson() => _$CharactersListEntityToJson(this);
}
