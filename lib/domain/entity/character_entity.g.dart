// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharacterEntity _$CharacterEntityFromJson(Map<String, dynamic> json) =>
    CharacterEntity()
      ..id = json['id'] as String?
      ..name = json['name'] as String?
      ..gender = json['gender'] as String?
      ..image = json['image'] as String?
      ..status = json['status'] as String?
      ..episode = (json['episode'] as List<dynamic>?)
          ?.map((e) => EpisodeEntity.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$CharacterEntityToJson(CharacterEntity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'gender': instance.gender,
      'image': instance.image,
      'status': instance.status,
      'episode': instance.episode,
    };
